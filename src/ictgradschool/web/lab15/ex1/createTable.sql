DROP TABLE IF EXISTS access_log;

CREATE TABLE access_log (
id INT AUTO_INCREMENT,
name VARCHAR(40),
description VARCHAR(200),
timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
PRIMARY KEY (id)
);

INSERT INTO access_log (name, description)
VALUES ('bob', 'descbob wev'),
('sally', 'descsally i dunno'),
('kim', 'what should go here')

INSERT INTO access_log (name, description)
VALUES ('laura', 'another boring description')
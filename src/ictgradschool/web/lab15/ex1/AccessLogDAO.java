package ictgradschool.web.lab15.ex1;

import javax.servlet.ServletContext;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class AccessLogDAO {
    Properties dbProps;

    public AccessLogDAO(ServletContext context){
        dbProps = new Properties();

        try{
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e){
            e.printStackTrace();
        }

        try (FileInputStream fIn = new FileInputStream(context.getRealPath("WEB-INF/db.properties"))) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //that returns a list of all AccessLog entries in the table,
    public List<AccessLog> allAccessLogs() {
System.out.println("in allaccesslogs dao");
        List<AccessLog> allAccessLogs = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            try (Statement stmt = conn.createStatement()) {
                try (ResultSet rs = stmt.executeQuery("SELECT * FROM access_log;")) {

                    while (rs.next()) {
                        System.out.println(rs.getInt(1));
                        AccessLog a = new AccessLog();
                        a.setId(rs.getInt(1));
                        a.setName(rs.getString(2));
                        a.setDescription(rs.getString(3));
                        a.setTimestamp(rs.getTimestamp(4).toString());
                        allAccessLogs.add(a);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allAccessLogs;

    }

    public void addAccessLog(AccessLog a) {




        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO access_log (name, description) VALUES (?, ?)")) {
                stmt.setString(1, a.getName());
                stmt.setString(2, a.getDescription());

                stmt.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

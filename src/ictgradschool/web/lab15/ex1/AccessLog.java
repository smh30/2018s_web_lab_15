package ictgradschool.web.lab15.ex1;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;

public class AccessLog implements Serializable {
    private int id;
    private String name;
    private String description;
    private String timestamp;

// this thing is just a POJO / Javabean
    public AccessLog(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }



}

package ictgradschool.web.lab15.ex1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class LoggingTableNewEntry extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO: Retrieve parameters and store new entries in the database
        // TODO: Redirect back to the LoggingTable

        String name = request.getParameter("name");
        String desc = request.getParameter("description");

        AccessLog a = new AccessLog();
        a.setName(name);
        a.setDescription(desc);

        AccessLogDAO dao = new AccessLogDAO(getServletContext());
        dao.addAccessLog(a);

//response.sendRedirect("/smh30_ex01/question1");
response.sendRedirect("/question1");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
    }
}

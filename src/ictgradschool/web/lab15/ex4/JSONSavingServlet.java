package ictgradschool.web.lab15.ex4;

import org.json.simple.JSONObject;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JSONSavingServlet extends HttpServlet {
    //private String uploadsFolder;
    private File uploadsFolder;

    /**
     * @param servletConfig a ServletConfig object containing information gathered when
     *                      the servlet was created, including information from web.xml
     */
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        //this.uploadsFolder = servletConfig.getInitParameter("json-save-directory");
        uploadsFolder = new File(getServletContext().getRealPath("/jsonTransactions"));
        if (!uploadsFolder.exists()){
            uploadsFolder.mkdirs();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Map<String, String[]> params = request.getParameterMap();

        for (String param : params.keySet()) {
            //System.out.println(param + ": " + params.get(param)[0]);
            if (params.get(param)[0].equals("")) {
                // that means that one of the params was empty

                System.out.println("an empty param found: " + param);


                doGet(request, response);
                //gotta use a return here, or else it will still attempt to complete the doPost even after the redirect
                return;
            }
        }


        System.out.println("all params full");
        //request.setAttribute("unfinished", false);

        // get the data from the params into the servlet as java variables
        int invoice;
        String address;
        String name;
        String cardType;
        String cardNum;

        //todo add checks that the data (esp ccNum) are the correct size/format
        invoice = Integer.parseInt(request.getParameter("invoiceNum"));
        address = request.getParameter("address");
        name = request.getParameter("name");
        cardType = request.getParameter("ccProvider");
        // card number is made up of 4 different fields..........
        cardNum = request.getParameter("ccNumber1") + "-" + request.getParameter("ccNumber2") + "-" +
                request.getParameter("ccNumber3") + "-" + request.getParameter("ccNumber4");

        //System.out.println(cardNum);





        JSONObject transaction = new JSONObject();

        // Adds a String value to the JSON
        transaction.put("billingAddress", address);

        JSONObject cc = new JSONObject();

        cc.put("cardName", name);
        cc.put("cardType", cardType);
        cc.put("cardNo", cardNum);

        transaction.put("creditCard", cc);


        File jsonFile = null;
        // Add the array to our object
        
       // get the upload folder, ensure it exists
        


        jsonFile = new File(uploadsFolder, invoice + ".json");
        
//        if (new File(uploadsFolder).isDirectory()) {
//System.out.println("saving locally");
//
//        } else {
//System.out.println("Saving to desktop from online");
//            String fileLocation = System.getProperty("user.home") + "/Desktop";
//        jsonFile = new File (fileLocation, invoice + ".json");
//
//        }

        boolean saved = saveJSONObject(jsonFile, transaction);
        
        System.out.println("saved: " + saved);
        //System.out.println("saved to " + jsonFile.getAbsolutePath());



doGet(request,response);
       // request.getRequestDispatcher("JSONForm.jsp").forward(request, response);
        //    response.sendRedirect("JSONForm.jsp");


        /* Save the JSON object to an appropriate directory, using the    *
         * 'invoice number' parameter as the filename, with the extension *
         * '.json'. An example filename might be '15678.json'. The method *
         * saveJSONObject() has been provided to save the JSON            */
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // redirects to show the blank JSP form page
        //response.sendRedirect("JSONForm.jsp");
        //request.setAttribute("invoice", request.getParameter("invoiceNum"));

        //this seems to work equivalently???
        // could i just have said:
//        if (done){
//            send w/o including forward??
        //is this a sendRedirect()??
//        }



        if (uploadsFolder.isDirectory()) {
        File[] files = uploadsFolder.listFiles();
        List<String> fileNames = new ArrayList<>();

        for (File file : files) {
            if (file.isFile()) {
                fileNames.add(file.getName());
            }
        }

        request.setAttribute("fileNames", fileNames);
        request.setAttribute("uploadsFolder", uploadsFolder.getName());
        //response.sendRedirect("JSONForm.jsp");
}
        request.getRequestDispatcher("JSONForm.jsp").forward(request, response);

    }

    /**
     * Writes the given JSONObject (in JSON format) to the specified file path
     *
     * @param file
     * @param jsonRecord
     * @return true if file written successfully, false otherwise
     */
    private boolean saveJSONObject(File file, JSONObject jsonRecord) {
        if (file.exists()) {
            System.out.println("file already exists here");
            return false;
        }

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(JSONObject.toJSONString(jsonRecord));
            System.out.println("saved apparently");
            System.out.println("saved to: " + file.toString());
        } catch (IOException e) {
            System.out.println("error with the IO");
            e.printStackTrace();
            return false;
        }

        return true;
    }
}

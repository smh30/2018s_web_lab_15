<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
    <head>
        <title>Exercise 01</title>
        <style>

            table {
                font-family: sans-serif;
            }

            thead {
                background-color: indianred;
                font-weight: bold;
            }

            tr:nth-child(even) {
                background-color: gainsboro;
            }

        </style>
    </head>
    <body>

        <form action="question1/new" method="POST">

            <label for="name">Name: </label> <input type="text" id="name" name="name"> <label for="desc">Description: </label>
            <input type="text" id="desc" name="description"> <br> <input type="submit" value="submit">


        </form>

        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Descriptions</th>
                    <th>Timestamp</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${allLogs}" var="log">
                <c:if test="${log.id % 30 == 0}">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Descriptions</th>
                    <th>Timestamp</th>
                </tr>
            </thead>
            </c:if>
            <tr>
                <td>${log.id}</td>
                <td>${log.name}</td>
                <td>${log.description}</td>
                <td>${log.timestamp}</td>


            </tr>


            </c:forEach>

            </tbody>
        </table>

    </body>
</html>

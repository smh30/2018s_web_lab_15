<%--<<<<<<< HEAD--%>
<%--&lt;%&ndash;--%>
  <%--Created by IntelliJ IDEA.--%>
  <%--User: stephaniehope--%>
  <%--Date: 17/01/19--%>
  <%--Time: 10:08 PM--%>
  <%--To change this template use File | Settings | File Templates.--%>
<%--&ndash;%&gt;--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%--<html>--%>
<%--<head>--%>
    <%--<title>JSON Form</title>--%>
<%--</head>--%>
<%--<body>--%>
<%--<!--todo check if this 'questin4' without a slash is correct-->--%>
<%--<form method="post" action="question4">--%>
    <%--<label for="invoicenum">Invoice number: </label>--%>
    <%--<input type="number" id="invoicenum" name="invoicenum"><br>--%>
    <%--<label for="address">Billing Address: </label>--%>
    <%--<textarea rows="6" cols="60" id="address" name="address"></textarea><br>--%>
    <%--<label for="ccName">Credit Card Name: </label>--%>
    <%--<input type="text" id="ccName" name="ccName"><br>--%>
    <%--<label for="ccProvider">Credit Card Provider: </label>--%>
    <%--<input type="text" id="ccProvider" name="ccProvider"><br>--%>
    <%--<label for="ccNumber">Credit Card Numger: </label>--%>
    <%--<input type="number" id="ccNumber" name="ccNum1">--%>
    <%--<input type="number"  name="ccNum2">--%>
    <%--<input type="number"  name="ccNum3">--%>
    <%--<input type="number"  name="ccNum4">--%>

    <%--<input type="submit" value="Submit Data">--%>
<%--</form>--%>


<%--=======--%>
<%@ page import="java.util.Map" %>
<%@ page import="java.io.File" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: smh30
  Date: 18/01/2019
  Time: 9:48 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%! private String invoicenum = "";
    private String address = "";
    private String name = "";
    private String ccProvider = "";
    private String ccNum1 = "";
    private String ccNum2 = "";
    private String ccNum3 = "";
    private String ccNum4 = "";
    private boolean prefill = false;
%>

<%if (request.getParameterMap().size()!=0){
    prefill = true;
    invoicenum=request.getParameter("invoiceNum");
    address=request.getParameter("address");
    name=request.getParameter("name");
    ccProvider=request.getParameter("ccProvider");
    ccNum1=request.getParameter("ccNumber1");
    ccNum2=request.getParameter("ccNumber2");
    ccNum3=request.getParameter("ccNumber3");
    ccNum4=request.getParameter("ccNumber4");
}
boolean allFinished = false;
if(!invoicenum.equals("")&& !address.equals("")&& !name.equals("")&& !ccProvider.equals("") &&!ccNum1.equals("")&&
!ccNum2.equals("")&& !ccNum3.equals("") &&!ccNum4.equals("")){
    allFinished = true;
    invoicenum = "";
    address = "";
    name = "";
    ccProvider="";
    ccNum1="";
    ccNum2="";
    ccNum3="";
    ccNum4="";
}
%>
<html>
    <head>
        <title>JSON Form</title>
        <style>
            <% if (prefill && !allFinished){%>
            input[value=""] {
                border: 1px solid red;
            }

            <%
            //turns out the address was several blank spaces, so gotta trim it first.
            //System.out.println("address is:" + address +"end");
                   if(address.trim().equals("")) {
                %>
            #address {
                border: 1px solid red
            }
            <%
            }
            } %>

        </style>
    </head>
    <body>
        <form method="post" action="question4">






            <fieldset><legend>Credit Card Form</legend>
            <label for="invoiceNum">Invoice Number: </label> <input type="text" id="invoiceNum" name="invoiceNum" value=<%=invoicenum%>><br>

            <label for="address">Billing Address: </label> <textarea rows="6" cols="60" id="address" name="address" ><%=address%></textarea><br>

            <label for="name">Credit Card Name: </label> <input type="text" id="name" name="name" value="<%=name%>"><br>

            <label for="ccProvider">Credit Card Provider: </label> <input type="text" id="ccProvider" name="ccProvider" value="<%=ccProvider%>"><br>

            <label for="ccNumber1">Credit Card Number: </label>
            <input type="number" minlength="4" maxlength="4" id="ccNumber1" name="ccNumber1" value="<%=ccNum1%>">
            <input type="number" minlength="4" maxlength="4" id="ccNumber2" name="ccNumber2" value="<%=ccNum2%>">
            <input type="number" minlength="4" maxlength="4" id="ccNumber3" name="ccNumber3" value="<%=ccNum3%>">
            <input type="number" minlength="4" maxlength="4" id="ccNumber4" name="ccNumber4" value="<%=ccNum4%>"><br>

            <input type="submit" value="Submit Form">
            </fieldset>

            <%--apparently maybe the right way to do this is to add a submit event handler w/ javascript??--%>
            <%--and then inside that check if the things are filled and set the css if relevant--%>

        </form>
        <%! List<String> fileNames = new ArrayList<>(); %>
        <%  if(request.getAttribute("fileNames")!= null){
            fileNames = (List<String>)request.getAttribute("fileNames");
            String saveDir = (String)request.getAttribute("uploadsFolder");
            if (fileNames.size()!=0){ %>
        <h2>Submitted Invoices (<%= fileNames.size()%>):</h2>
        <ul>

        <%
        //System.out.println(fileNames);
            for (String fileName : fileNames){
                String link = saveDir +"\\" + fileName;
        %>
            <li><a href="<%= link %>"> <%= fileName%></a></li>
             <%
            }
          %>  </ul> <%
                 }}
       %>


</body>

</html>
